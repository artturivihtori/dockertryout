README
======

This project is to perform given assignment: World manager service.

The service is World management REST application. It is build using Spring boot application framework
and is run standalone or in Docker container.

Build 
======

Java:
Project is build by maven: mvn clean install

Deployment
==========

Before deployment copy assignment.jar to docker directory:
cp target/assignment.jar target/docker/

Docker:
Nagivate to directory [project-root]/target/docker/

Build using either of following commands:
* Build the image to container: docker build -t assignment .
* Build as multi container: docker-compose build --no-cache

Java: No separate deployment needed. Just copy the assignment.jar to suitable location.

Run
====

Project can be run locally: mvn spring-boot:run

World management service is provided at localhost:8080 despite the deployment.

Service can be run also as standlone mode: java -jar assignment.jar.

Docker:
Nagivate to directory [project-root]/target/docker/

Service can be run in docker container: docker run -d -p 8080:8080 --name assignment assignment
or via composer: docker-compose up. The selected method is dependend on the used deployment manner.
See deployment section.

To test the service navigate to URL http://localhost:8080/continents. It will print out
default data supplied at application startup.

REST-API
========
API provides following end points:
[base_url]/continents
[base_url]/countries

End points will list all data related to endpoint.

Manange continents:

List all: [base_url]/continents
List by id:  [base_url]/continents/{id} e.g. /1.
Add continent HTTP.POST: [base_url]/continents with payload: {"name":"Australia"}
Update continent HTTP.PUT: [base_url]/continents/3 with payload: {"name":"Australia2"}
	If the continent doesn't exists with the id 3, new continent will be created using the id.
Remove continent HTTP.DELETE: [base_url]/continents/3 will remove continent with id 3.

Manage countries:

Countries are managed same manner than continents. Only URL changes to countries
and of course data is country object specific: {"name":"Norway", "continentId":1}.
As it can be seen from the example object, each country will be assigned to own continent.
Assignment is mandatory and country can only be assigned to one continent.
Country cannot be added to non-existent continent.

Example request:
Add country HTTP.POST: [base_url]/countries with payload: {"name":"Norway", "continentId":1}.


Manage cities:

Cities are managed same manner than countries. Only URL changes to cities
and of course data is city object specific: {"name":"Oulu", "countryId":1}.
As it can be seen from the example object, each city will be assigned to own country.
Assignment is mandatory and city can only be assigned to one country.
City cannot be added to non-existent country.

Example request:
Add city HTTP.POST: [base_url]/cities with payload: {"name":"Oulu", "countryId":1}.


Further details with actual curl examples please refer to tests/manualTestCases.txt document.

Contact
=======
Please report bugs and development ideas to kalle.laamanen@gmail.com

