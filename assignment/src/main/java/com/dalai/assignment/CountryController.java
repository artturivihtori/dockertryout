package com.dalai.assignment;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
class CountryController {

	@Autowired
	private CountryRepository repository;

	@Autowired
	private ContinentRepository continentRepository;

	CountryController() {
	}
	
	CountryController(CountryRepository repository) {
		this.repository = repository;
	}
	
	@RequestMapping(value = "/countries", method = RequestMethod.GET)
	List<Country> all() {
		return repository.findAll();
	}
	
	@RequestMapping(value = "/countriesbycontinent/{continentId}", method = RequestMethod.GET)
	List<Country> countryByContinent(@PathVariable Long continentId) {
		return repository.findAll().stream().filter(country -> country.getContinentId() == continentId)
				.collect(Collectors.toList());
	}
	
	@RequestMapping(value = "/countries", method = RequestMethod.POST)
	Country newCountry(@RequestBody Country newCountry) {
		// This should be done using foreign key, but could not get it work
		// with in-memory db.
		
		validate(newCountry);
		return repository.save(newCountry);
	}
	
	/**
	 * Validate country.
	 * 
	 * Country must have assigned to existing continent.
	 * 
	 * @param newCountry to be validated.
	 * @throws CountryNotFoundException if new country is not valid.
	 */
	private void validate(Country newCountry) {
		
		if (!continentRepository.exists(newCountry.getContinentId())) {
			throw new CountryNotFoundException("Cannot assign country to non-existent continent", newCountry.getContinentId());
		}
	}

	@RequestMapping(value = "/countries/{id}", method = RequestMethod.GET)
	Country one(@PathVariable Long id) {
		Country match = repository.findOne(id);
		if (match == null) {
			throw new CountryNotFoundException(id);
		}
		return match;
	}
	
	@RequestMapping(value = "/countries/{id}", method = RequestMethod.PUT)
	Country replaceCountry(@RequestBody Country newCountry, @PathVariable Long id) {
		Country match = repository.findOne(id);

		validate(newCountry);
		
		if (match != null) {
			match.setName(newCountry.getName());
			return repository.save(match);
		} else {
			// New entry. Create with id.
			newCountry.setId(id);
			return repository.save(newCountry);
			
		}
	}
	
	@RequestMapping(value = "/countries/{id}", method = RequestMethod.DELETE)
	void deleteCountry(@PathVariable Long id) {
		repository.delete(id);
	}
}
