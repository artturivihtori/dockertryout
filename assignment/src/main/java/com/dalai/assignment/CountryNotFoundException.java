package com.dalai.assignment;

@SuppressWarnings("serial")
class CountryNotFoundException extends RuntimeException {
	CountryNotFoundException(Long id) {
		super("Could not find country " + id);
	}
	
	public CountryNotFoundException(String message, Long id) {
		super(message + ": " + id);
	}
}
