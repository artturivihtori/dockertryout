package com.dalai.assignment;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class City {

	private @Id @GeneratedValue Long id;
	private Long countryId;
	private String name;

	City() {
	}

	City(String name, Long countryId) {
		this.name = name;
		this.countryId = countryId;
	}

	public Long getId() {
		return this.id;
	}

	public Long getCountryId() {
		return this.countryId;
	}

	public String getName() {
		return this.name;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}

		if (!(o instanceof City)) {
			return false;
		}

		City city = (City) o;
		return Objects.equals(this.id, city.id) && Objects.equals(this.name, city.name)
				&& Objects.equals(this.countryId, city.countryId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.countryId, this.name);
	}

	@Override
	public String toString() {
		return "City{" + "id=" + this.id + " country=" + countryId + ", name='" + this.name + "\'}";
	}
}
