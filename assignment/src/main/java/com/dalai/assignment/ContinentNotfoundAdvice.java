package com.dalai.assignment;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ContinentNotfoundAdvice {

	  @ResponseBody
	  @ExceptionHandler(ContinentNotFoundException.class)
	  @ResponseStatus(HttpStatus.NOT_FOUND)
	  String continentNotFoundHandler(ContinentNotFoundException ex) {
	    return ex.getMessage();
	  }
}
