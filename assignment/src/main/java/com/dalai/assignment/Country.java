package com.dalai.assignment;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Country {

	private @Id @GeneratedValue Long id;
	private Long continentId;
	private String name;

	Country() {
	}

	Country(String name, Long continentId) {
		this.name = name;
		this.continentId = continentId;
	}

	public Long getId() {
		return this.id;
	}

	public Long getContinentId() {
		return this.continentId;
	}

	public String getName() {
		return this.name;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setContinentId(Long continentId) {
		this.continentId = continentId;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}

		if (!(o instanceof Country)) {
			return false;
		}

		Country country = (Country) o;
		return Objects.equals(this.id, country.id) && Objects.equals(this.name, country.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.continentId, this.name);
	}

	@Override
	public String toString() {
		return "Country{" + "id=" + this.id + " continent=" + continentId + ", name='" + this.name + "\'}";
	}
}
