package com.dalai.assignment;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
class CityController {

	@Autowired
	private CityRepository repository;

	@Autowired
	private CountryRepository continentRepository;

	CityController() {
	}
	
	CityController(CityRepository repository) {
		this.repository = repository;
	}
	
	@RequestMapping(value = "/cities", method = RequestMethod.GET)
	List<City> all() {
		return repository.findAll();
	}
	
	@RequestMapping(value = "/citiesbycountry/{countryId}", method = RequestMethod.GET)
	List<City> cityByCountry(@PathVariable Long countryId) {
		return repository.findAll().stream().filter(city -> city.getCountryId() == countryId)
				.collect(Collectors.toList());
	}
	
	@RequestMapping(value = "/cities", method = RequestMethod.POST)
	City newCity(@RequestBody City newCity) {
		// This should be done using foreign key, but could not get it work
		// with in-memory db.
		
		validate(newCity);
		return repository.save(newCity);
	}
	
	/**
	 * Validate city.
	 * 
	 * Country must have assigned to existing continent.
	 * 
	 * @param newCity to be validated.
	 * @throws CountryNotFoundException if new city is not valid.
	 */
	private void validate(City newCity) {
		
		if (!continentRepository.exists(newCity.getCountryId())) {
			throw new CountryNotFoundException("Cannot assign city to non-existent country", newCity.getCountryId());
		}
	}

	@RequestMapping(value = "/cities/{id}", method = RequestMethod.GET)
	City one(@PathVariable Long id) {
		City match = repository.findOne(id);
		if (match == null) {
			throw new CountryNotFoundException(id);
		}
		return match;
	}
	
	@RequestMapping(value = "/cities/{id}", method = RequestMethod.PUT)
	City replaceCity(@RequestBody City newCity, @PathVariable Long id) {
		City match = repository.findOne(id);

		validate(newCity);
		
		if (match != null) {
			match.setName(newCity.getName());
			return repository.save(match);
		} else {
			// New entry. Create with id.
			newCity.setId(id);
			return repository.save(newCity);
		}
	}
	
	@RequestMapping(value = "/cities/{id}", method = RequestMethod.DELETE)
	void deleteCity(@PathVariable Long id) {
		repository.delete(id);
	}
}
