package com.dalai.assignment;

@SuppressWarnings("serial")
class ContinentNotFoundException extends RuntimeException {
	ContinentNotFoundException(Long id) {
		super("Could not find continent " + id);
	}
}
