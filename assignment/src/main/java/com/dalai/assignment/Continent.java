package com.dalai.assignment;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Continent {
	
	private @Id @GeneratedValue Long id;
	private String name;
	
	Continent() {}
	
	Continent(String name) {
		this.name = name;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object o) {

	    if (this == o) {
	        return true;
	    }
	    
	    if (!(o instanceof Continent)) {
	      return false;
	    }
	    
	    Continent continent = (Continent) o;
	    return Objects.equals(this.id, continent.id) && Objects.equals(this.name, continent.name);
    }

	  @Override
	  public int hashCode() {
	    return Objects.hash(this.id, this.name);
	  }

	  @Override
	  public String toString() {
	    return "Continent{" + "id=" + this.id + ", name='" + this.name + '\'' + '}';
	  }
}
