package com.dalai.assignment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {

	private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);
	
	  @Bean
		CommandLineRunner initDatabase(ContinentRepository continentRepository, CountryRepository countryRepository,
				CityRepository cityRepository) {

		  return args -> {
	      log.info("Preloading continent " + continentRepository.save(new Continent("Europe")));
	      log.info("Preloading continent " + continentRepository.save(new Continent("Asia")));
	      log.info("Preloading country " + countryRepository.save(new Country("Finland", new Long(1))));
	      log.info("Preloading country " + countryRepository.save(new Country("Sweden", new Long(1))));
	      log.info("Preloading country " + countryRepository.save(new Country("Japan", new Long(2))));
	      log.info("Preloading city " + cityRepository.save(new City("Helsinki", new Long(1))));
	      log.info("Preloading city " + cityRepository.save(new City("Tampere", new Long(1))));
	      log.info("Preloading city " + cityRepository.save(new City("Stocholm", new Long(2))));
	    };
	  }
}
