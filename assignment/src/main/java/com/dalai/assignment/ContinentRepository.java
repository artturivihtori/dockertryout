package com.dalai.assignment;

import org.springframework.data.jpa.repository.JpaRepository;

interface ContinentRepository extends JpaRepository<Continent, Long> {

}
