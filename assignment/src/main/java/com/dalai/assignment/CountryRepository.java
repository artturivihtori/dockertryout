package com.dalai.assignment;

import org.springframework.data.jpa.repository.JpaRepository;

interface  CountryRepository extends JpaRepository<Country, Long> {

}
