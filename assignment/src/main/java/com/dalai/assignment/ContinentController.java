package com.dalai.assignment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
class ContinentController {

	@Autowired
	private ContinentRepository repository;
	
	
	ContinentController() {
	}
	
	ContinentController(ContinentRepository repository) {
		this.repository = repository;
	}
	
	@RequestMapping(value = "/continents", method = RequestMethod.GET)
	List<Continent> all() {
		return repository.findAll();
	}
	
	@RequestMapping(value = "/continents", method = RequestMethod.POST)
	Continent newContinent(@RequestBody Continent newContinent) {
		return repository.save(newContinent);
	}
	
	@RequestMapping(value = "/continents/{id}", method = RequestMethod.GET)
	Continent one(@PathVariable Long id) {
		Continent match = repository.findOne(id);
		if (match == null) {
			throw new ContinentNotFoundException(id);
		}
		return match;
	}
	
	@RequestMapping(value = "/continents/{id}", method = RequestMethod.PUT)
	Continent replaceContinent(@RequestBody Continent newContinent, @PathVariable Long id) {
		Continent match = repository.findOne(id);
		
		if (match != null) {
			match.setName(newContinent.getName());
			return repository.save(match);
		} else {
			// New entry. Create with id.
			newContinent.setId(id);
			return repository.save(newContinent);
			
		}
	}
	
	@RequestMapping(value = "/continents/{id}", method = RequestMethod.DELETE)
	void deleteContinent(@PathVariable Long id) {
		repository.delete(id);
	}
}
